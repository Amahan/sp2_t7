#include "slopeMeasureDev.h"
#include "GL\glew.h"
#include <math.h>   //i need this to find angle needs to turn
#include "shader.hpp"
#include "Mtx44.h"
#include "Application.h"
#include "Light.h"
#include "Utility.h"
#include "LoadTGA.h"
#include "Mesh.h"
#include <math.h>
slopeMeasureDev::slopeMeasureDev()
{
}

slopeMeasureDev::~slopeMeasureDev()
{
}

void slopeMeasureDev::Init()
{
	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);
	timer0 = 5;
	dogLeg = 0;
	timer0Rev = false;
	dogLegRev = false;
	toggle = true;
	dogMove = true;
	buttonTranslate = 0;

	dogGoalView = (camera.position - dogPos).Normalized();
	dogGoalView.y = 0;
	dogPos = (0, -10, 0);
	dogTarget = dogGoalView;
	dogTarget.y = 0;
	dogTarget.z *= -1;
	dogTarget = dogTarget.Normalized();
	doggyRotate = 0;
	doorOpen = true;
	lampostOn = true;
	buttonSleep = -0.99; //timer to let user unpress button
	dogView = dogTarget;

	revTorch = false;
	torchlight = 60;

	dogView = dogView.Normalized();
	// Generate a default VAO for now
	glGenVertexArrays(1, &m_vertexArrayID);
	glBindVertexArray(m_vertexArrayID);
	glEnable(GL_CULL_FACE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	camera.Init(Vector3(0, 10.2885, 0), Vector3(0, 0, -1), Vector3(0, 1, 0));

	Mtx44 projection;
	projection.SetToPerspective(45.f, 4.f / 3.f, 0.1f, 1000.f);
	projectionStack.LoadMatrix(projection);

	//Load vertex and fragment shaders
	m_programID = LoadShaders("Shader//TransformVertexShader.vertexshader", "Shader//SimpleFragmentShader.fragmentshader");
	m_programID = LoadShaders("Shader//Shading.vertexshader", "Shader//Shading.fragmentshader");
	m_programID = LoadShaders("Shader//Shading.vertexshader", "Shader//LightSource.fragmentshader");
	m_programID = LoadShaders("Shader//Texture.vertexshader", "Shader//Texture.fragmentshader");
	m_programID = LoadShaders("Shader//Texture.vertexshader", "Shader//Blending.fragmentshader");
	m_programID = LoadShaders("Shader//Texture.vertexshader", "Shader//Text.fragmentshader");
	m_parameters[U_NUMLIGHTS] = glGetUniformLocation(m_programID, "numLights");
	m_parameters[U_LIGHT0_TYPE] = glGetUniformLocation(m_programID, "lights[0].type");
	m_parameters[U_LIGHT0_SPOTDIRECTION] = glGetUniformLocation(m_programID, "lights[0].spotDirection");
	m_parameters[U_LIGHT0_COSCUTOFF] = glGetUniformLocation(m_programID, "lights[0].cosCutoff");
	m_parameters[U_LIGHT0_COSINNER] = glGetUniformLocation(m_programID, "lights[0].cosInner");
	m_parameters[U_LIGHT0_EXPONENT] = glGetUniformLocation(m_programID, "lights[0].exponent");
	m_parameters[U_MODELVIEW_INVERSE_TRANSPOSE] = glGetUniformLocation(m_programID, "MV_inverse_transpose");

	m_parameters[U_MATERIAL_AMBIENT] = glGetUniformLocation(m_programID, "material.kAmbient");
	m_parameters[U_MATERIAL_DIFFUSE] = glGetUniformLocation(m_programID, "material.kDiffuse");
	m_parameters[U_MATERIAL_SPECULAR] = glGetUniformLocation(m_programID, "material.kSpecular");
	m_parameters[U_MATERIAL_SHININESS] = glGetUniformLocation(m_programID, "material.kShininess");
	m_parameters[U_LIGHT0_POSITION] = glGetUniformLocation(m_programID, "lights[0].position_cameraspace");
	m_parameters[U_LIGHT0_COLOR] = glGetUniformLocation(m_programID, "lights[0].color");
	m_parameters[U_LIGHT0_POWER] = glGetUniformLocation(m_programID, "lights[0].power");
	m_parameters[U_LIGHT0_KC] = glGetUniformLocation(m_programID, "lights[0].kC");
	m_parameters[U_LIGHT0_KL] = glGetUniformLocation(m_programID, "lights[0].kL");
	m_parameters[U_LIGHT0_KQ] = glGetUniformLocation(m_programID, "lights[0].kQ");
	m_parameters[U_LIGHTENABLED] = glGetUniformLocation(m_programID, "lightEnabled");

	m_parameters[U_COLOR_TEXTURE_ENABLED] = glGetUniformLocation(m_programID, "colorTextureEnabled");
	m_parameters[U_COLOR_TEXTURE] = glGetUniformLocation(m_programID, "colorTexture");
	m_parameters[U_MVP] = glGetUniformLocation(m_programID, "MVP");
	m_parameters[U_MODELVIEW] = glGetUniformLocation(m_programID, "MV");

	m_parameters[U_TEXT_ENABLED] = glGetUniformLocation(m_programID, "textEnabled");
	m_parameters[U_TEXT_COLOR] = glGetUniformLocation(m_programID, "textColor");

	m_parameters[U_LIGHT1_POSITION] = glGetUniformLocation(m_programID, "lights[1].position_cameraspace");
	m_parameters[U_LIGHT1_COLOR] = glGetUniformLocation(m_programID, "lights[1].color");
	m_parameters[U_LIGHT1_POWER] = glGetUniformLocation(m_programID, "lights[1].power");
	m_parameters[U_LIGHT1_KC] = glGetUniformLocation(m_programID, "lights[1].kC");
	m_parameters[U_LIGHT1_KL] = glGetUniformLocation(m_programID, "lights[1].kL");
	m_parameters[U_LIGHT1_KQ] = glGetUniformLocation(m_programID, "lights[1].kQ");
	m_parameters[U_LIGHT1_TYPE] = glGetUniformLocation(m_programID, "lights[1].type");
	m_parameters[U_LIGHT1_SPOTDIRECTION] = glGetUniformLocation(m_programID, "lights[1].spotDirection");
	m_parameters[U_LIGHT1_COSCUTOFF] = glGetUniformLocation(m_programID, "lights[1].cosCutoff");
	m_parameters[U_LIGHT1_COSINNER] = glGetUniformLocation(m_programID, "lights[1].cosInner");
	m_parameters[U_LIGHT1_EXPONENT] = glGetUniformLocation(m_programID, "lights[1].exponent");

	m_parameters[U_LIGHT2_POSITION] = glGetUniformLocation(m_programID, "lights[2].position_cameraspace");
	m_parameters[U_LIGHT2_COLOR] = glGetUniformLocation(m_programID, "lights[2].color");
	m_parameters[U_LIGHT2_POWER] = glGetUniformLocation(m_programID, "lights[2].power");
	m_parameters[U_LIGHT2_KC] = glGetUniformLocation(m_programID, "lights[2].kC");
	m_parameters[U_LIGHT2_KL] = glGetUniformLocation(m_programID, "lights[2].kL");
	m_parameters[U_LIGHT2_KQ] = glGetUniformLocation(m_programID, "lights[2].kQ");
	m_parameters[U_LIGHT2_TYPE] = glGetUniformLocation(m_programID, "lights[2].type");
	//m_parameters[U_LIGHT2_SPOTDIRECTION] = glGetUniformLocation(m_programID, "lights[2].spotDirection");
	//m_parameters[U_LIGHT2_COSCUTOFF] = glGetUniformLocation(m_programID, "lights[2].cosCutoff");
	//m_parameters[U_LIGHT2_COSINNER] = glGetUniformLocation(m_programID, "lights[2].cosInner");
	//m_parameters[U_LIGHT2_EXPONENT] = glGetUniformLocation(m_programID, "lights[2].exponent");

	glUseProgram(m_programID);

	// Get a handle for our "MVP" uniform
	m_parameters[U_MVP] = glGetUniformLocation(m_programID, "MVP");

	// Enable depth test
	glEnable(GL_DEPTH_TEST);

	for (int i = 0; i < NUM_GEOMETRY; ++i)
	{
		meshList[i] = NULL;
	}

	meshList[GEO_AXES] = MeshBuilder::GenerateAxes("reference", 1000, 1000, 1000);

	meshList[GEO_LIGHTBALL] = MeshBuilder::GenerateSphere("lightball", Color(1.0f, 0.0f, 0.0f), 18, 36, 1);

	meshList[GEO_FRONT] = MeshBuilder::GenerateQuad("front", Color(0.0f, 0.0f, 0.0f), 1, 1);
	meshList[GEO_FRONT]->textureID = LoadTGA("Image//front.tga");

	meshList[GEO_BACK] = MeshBuilder::GenerateQuad("back", Color(0.0f, 0.0f, 0.0f), 1, 1);
	meshList[GEO_BACK]->textureID = LoadTGA("Image//back.tga");

	meshList[GEO_TOP] = MeshBuilder::GenerateQuad("top", Color(0.0f, 0.0f, 0.0f), 1, 1);
	meshList[GEO_TOP]->textureID = LoadTGA("Image//top.tga");

	meshList[GEO_BOTTOM] = MeshBuilder::GenerateQuad("bottom", Color(0.0f, 0.0f, 0.0f), 1, 1);
	meshList[GEO_BOTTOM]->textureID = LoadTGA("Image//bottom.tga");

	meshList[GEO_LEFT] = MeshBuilder::GenerateQuad("left", Color(0.0f, 0.0f, 0.0f), 1, 1);
	meshList[GEO_LEFT]->textureID = LoadTGA("Image//left.tga");

	meshList[GEO_RIGHT] = MeshBuilder::GenerateQuad("right", Color(0.0f, 0.0f, 0.0f), 1, 1);
	meshList[GEO_RIGHT]->textureID = LoadTGA("Image//right.tga");

	meshList[GEO_MODEL_LAND] = MeshBuilder::GenerateOBJ("land", "OBJ//basic_land.obj");
	meshList[GEO_MODEL_LAND]->textureID = LoadTGA("Image//basic_land.tga");

	meshList[GEO_MODEL_DOOR] = MeshBuilder::GenerateOBJ("door", "OBJ//door.obj");
	meshList[GEO_MODEL_DOOR]->textureID = LoadTGA("Image//door.tga");

	meshList[GEO_MODEL_DOGBODY] = MeshBuilder::GenerateOBJ("dogbody", "OBJ//dogBody.obj");
	meshList[GEO_MODEL_DOGBODY]->textureID = LoadTGA("Image//dogBody.tga");

	meshList[GEO_MODEL_DOGLEG] = MeshBuilder::GenerateOBJ("dogleg", "OBJ//dogLeg.obj");
	meshList[GEO_MODEL_DOGLEG]->textureID = LoadTGA("Image//dogleg.tga");

	meshList[GEO_MODEL_DOGHEAD] = MeshBuilder::GenerateOBJ("doghead", "OBJ//dogHead.obj");
	meshList[GEO_MODEL_DOGHEAD]->textureID = LoadTGA("Image//doghead.tga");

	meshList[GEO_MODEL_DOGLEFTEAR] = MeshBuilder::GenerateOBJ("dogleftear", "OBJ//dogLeftEar.obj");
	meshList[GEO_MODEL_DOGLEFTEAR]->textureID = LoadTGA("Image//dogears.tga");

	meshList[GEO_MODEL_DOGRIGHTEAR] = MeshBuilder::GenerateOBJ("dogrightear", "OBJ//dogRightEar.obj");
	meshList[GEO_MODEL_DOGRIGHTEAR]->textureID = LoadTGA("Image//dogears.tga");

	meshList[GEO_MODEL_HOUSE] = MeshBuilder::GenerateOBJ("shoe", "OBJ//house.obj");
	meshList[GEO_MODEL_HOUSE]->textureID = LoadTGA("Image//house.tga");

	meshList[GEO_MODEL_BUTTON] = MeshBuilder::GenerateOBJ("button", "OBJ//button.obj");
	meshList[GEO_MODEL_BUTTON]->textureID = LoadTGA("Image//button.tga");

	meshList[GEO_MODEL_BUTTONBASE] = MeshBuilder::GenerateOBJ("buttonbase", "OBJ//buttonbase.obj");
	meshList[GEO_MODEL_BUTTONBASE]->textureID = LoadTGA("Image//buttonbase.tga");

	meshList[GEO_MODEL_WALL] = MeshBuilder::GenerateOBJ("wall", "OBJ//wall.obj");
	meshList[GEO_MODEL_WALL]->textureID = LoadTGA("Image//wall.tga");

	meshList[GEO_MODEL_SLOPE] = MeshBuilder::GenerateOBJ("wall", "OBJ//slope.obj");

	meshList[GEO_MODEL_LAMPOST] = MeshBuilder::GenerateOBJ("lampost", "OBJ//lampost.obj");
	meshList[GEO_MODEL_LAMPOST]->textureID = LoadTGA("Image//Lampost.tga");

	meshList[GEO_MODEL_BONE] = MeshBuilder::GenerateOBJ("bone", "OBJ//bone.obj");
	meshList[GEO_MODEL_BONE]->textureID = LoadTGA("Image//bone.tga");

	meshList[GEO_TEXT] = MeshBuilder::GenerateText("text", 16, 16);
	meshList[GEO_TEXT]->textureID = LoadTGA("Image//calibri.tga");

	meshList[GEO_DOORTEXT] = MeshBuilder::GenerateText("text", 16, 16);
	meshList[GEO_DOORTEXT]->textureID = LoadTGA("Image//calibri.tga");

	meshList[GEO_LAMPTEXT] = MeshBuilder::GenerateText("text", 16, 16);
	meshList[GEO_LAMPTEXT]->textureID = LoadTGA("Image//calibri.tga");

	glGenerateMipmap(GL_TEXTURE_2D);
	
	light[0].type = Light::LIGHT_SPOT;
	light[0].position.Set(0, 175, 0);
	light[0].color.Set(1, 1, 1);
	light[0].power = 20;
	light[0].kC = 1.0f;
	light[0].kL = 0.01f;
	light[0].kQ = 0.001f;
	light[0].cosCutoff = cos(Math::DegreeToRadian(45));
	light[0].cosInner = cos(Math::DegreeToRadian(45));
	light[0].exponent = 3.f;
	light[0].spotDirection.Set(0.f, 1.f, 0.f);

	light[1].type = Light::LIGHT_SPOT;
	light[1].position.Set(20, 25, 0);
	light[1].color.Set(0.882, 0.905, 0.294);
	light[1].power = 5;
	light[1].kC = 1.f;
	light[1].kL = 0.01f;
	light[1].kQ = 0.001f;
	light[1].cosCutoff = cos(Math::DegreeToRadian(torchlight));
	light[1].cosInner = cos(Math::DegreeToRadian(30));
	light[1].exponent = 3.f;
	light[1].spotDirection.Set(0.f, 1.f, 0.f);

	glUniform1i(m_parameters[U_LIGHT0_TYPE], light[0].type);
	glUniform3fv(m_parameters[U_LIGHT0_COLOR], 1, &light[0].color.r);
	glUniform1f(m_parameters[U_LIGHT0_POWER], light[0].power);
	glUniform1f(m_parameters[U_LIGHT0_KC], light[0].kC);
	glUniform1f(m_parameters[U_LIGHT0_KL], light[0].kL);
	glUniform1f(m_parameters[U_LIGHT0_KQ], light[0].kQ);
	glUniform1f(m_parameters[U_LIGHT0_COSCUTOFF], light[0].cosCutoff);
	glUniform1f(m_parameters[U_LIGHT0_COSINNER], light[0].cosInner);
	glUniform1f(m_parameters[U_LIGHT0_EXPONENT], light[0].exponent);

	glUniform1i(m_parameters[U_LIGHT1_TYPE], light[1].type);
	glUniform3fv(m_parameters[U_LIGHT1_COLOR], 1, &light[1].color.r);
	glUniform1f(m_parameters[U_LIGHT1_POWER], light[1].power);
	glUniform1f(m_parameters[U_LIGHT1_KC], light[1].kC);
	glUniform1f(m_parameters[U_LIGHT1_KL], light[1].kL);
	glUniform1f(m_parameters[U_LIGHT1_KQ], light[1].kQ);
	glUniform1f(m_parameters[U_LIGHT1_COSCUTOFF], light[1].cosCutoff);
	glUniform1f(m_parameters[U_LIGHT1_COSINNER], light[1].cosInner);
	glUniform1f(m_parameters[U_LIGHT1_EXPONENT], light[1].exponent);
	
	glUniform1i(m_parameters[U_NUMLIGHTS], 2);

	float maxAnisotropy = 1.f;
	glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &maxAnisotropy);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, (GLint)maxAnisotropy);
}

void slopeMeasureDev::Update(double dt)
{
	if (!revTorch)
	{
		if (torchlight < 70)
			torchlight += 100 * dt;
		else
			revTorch = true;
	}
	else if (revTorch)
	{
		if (torchlight > 0)
			torchlight -= 100 * dt;
		else
			revTorch = false;
	}
	//light[1].cosCutoff = cos(Math::DegreeToRadian(torchlight));
	//glUniform1f(m_parameters[U_LIGHT1_COSCUTOFF], light[1].cosCutoff);
	light[1].color.Set(1 - torchlight * 0.1, torchlight * 0.1, 0.294);
	glUniform3fv(m_parameters[U_LIGHT1_COLOR], 1, &light[1].color.r);

	//for my lampost trigger, the button will move down/up depending on whether it's on or off
	if (lampostOn)
	{
		if (buttonTranslate > -0.04)
			buttonTranslate -= (float)(0.1 * dt);
	}
	else if (buttonTranslate < -0.01)
		buttonTranslate += (float)(0.1 * dt);


	if (!boneThrown)
	{
		dogGoalView = (camera.position - dogPos).Normalized();
		if (Application::IsKeyPressed(VK_LBUTTON))
		{
			dogBoneDir = camera.view;
			boneThrown = true;
		}
	}
	else
	{
		if (dogBone.y > 1)
		{
			dogBone += dogBoneDir * (float)(20 * dt);	//bone flying towards where it's thrown
			dogBoneDir.y -= (float)(0.2 * dt);		//bone goes lower every time this runs
		}
		dogGoalView = (dogBone - dogPos).Normalized();
		if ((dogPos - dogBone).Length() <= 10 && dogBone.y <=1)
			boneThrown = false; //if the dog goes close enough, the bone is considered not thrown and returned to user
		else
		{
			dogPos.x += dogGoalView.x * (float)(10.f * dt);
			dogPos.z += dogGoalView.z * (float)(10.f * dt);
		}
	}

		dogGoalView.y = 0;

		//dogtarget acts as the north of the dog, comparing to the view vector from dog to its target (owner or bone) to get angle to turn
	goalAngle =
		(dogTarget.Dot(dogGoalView)) 
			/
		(sqrt(dogTarget.x * dogTarget.x + dogTarget.z * dogTarget.z) * sqrt(dogGoalView.x * dogGoalView.x + dogGoalView.z * dogGoalView.z) )
	;
	goalAngle = acos(goalAngle) * 180 / Math::PI;
		//first quartile
		if (dogGoalView.x > 0 && dogGoalView.z < 0)
			doggyRotate = -(90 - goalAngle);
		//second quartile
		else if (dogGoalView.x > 0 && dogGoalView.z > 0)
			doggyRotate = -(90 + goalAngle);
		//third quartile
		else if (dogGoalView.x < 0 && dogGoalView.z > 0)
			doggyRotate = -(180 + goalAngle - 90);
		//fourth quartile
		else if (dogGoalView.x < 0 && dogGoalView.z < 0)
			doggyRotate = -(270 + (180 - goalAngle));
	if((!boneThrown && (camera.position - dogPos).Length() > 12) || boneThrown)
	{
		++dogMove;
		dogPos.x += dogGoalView.x * (float)(10.f * dt);
		dogPos.z += dogGoalView.z * (float)(10.f * dt);
	}
	else
		dogMove = false; //dont move if it stops in front of user

	if (!dogMove)
	{
		dogLeg = 0;
	}

	fps = 1.0/dt;
	if (!doorOpen)
	{
		if (timer0 < 0)
			timer0 += (float)(70 * dt);
	}
	else
	{
		if (timer0 > -90)
			timer0 -= (float)(70 * dt);
	}

	if (dogLegRev == true)
	{
		dogLeg -= (float)(5.0 * dt);

		if (dogLeg < -1)
			dogLegRev = false;
	}
	else
	{
		dogLeg += (float)(5.0 * dt);

		if (dogLeg > 1)
			dogLegRev = true;
	}

	static const float LSPEED = 5.0f;

	if (Application::IsKeyPressed('1'))
	{
		glEnable(GL_CULL_FACE);
	}
	if (Application::IsKeyPressed('2'))
	{
		glDisable(GL_CULL_FACE);
	}
	if (Application::IsKeyPressed('3'))
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
	if (Application::IsKeyPressed('4'))
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}

	//movement for lightball 1
	if (Application::IsKeyPressed('I'))
	{
		light[0].position.z -= (float)(LSPEED * dt);
	}
	if (Application::IsKeyPressed('K'))
	{
		light[0].position.z += (float)(LSPEED * dt);
	}
	if (Application::IsKeyPressed('J'))
	{
		light[0].position.x -= (float)(LSPEED * dt);
	}
	if (Application::IsKeyPressed('L'))
	{
		light[0].position.x += (float)(LSPEED * dt);
	}
	if (Application::IsKeyPressed('O'))
	{
		light[0].position.y -= (float)(LSPEED * dt);
	}
	if (Application::IsKeyPressed('P'))
	{
		light[0].position.y += (float)(LSPEED * dt);
	}

	//lightball 1 type
	if (Application::IsKeyPressed('Z'))
	{
		light[0].type = Light::LIGHT_POINT;
		glUniform1i(m_parameters[U_LIGHT0_TYPE], light[0].type);
	}
	else if (Application::IsKeyPressed('X'))
	{
		light[0].type = Light::LIGHT_DIRECTIONAL;
		glUniform1i(m_parameters[U_LIGHT0_TYPE], light[0].type);
	}
	else if (Application::IsKeyPressed('C'))
	{
		light[0].type = Light::LIGHT_SPOT;
		glUniform1i(m_parameters[U_LIGHT0_TYPE], light[0].type);
	}

	if (Application::IsKeyPressed('9'))
	{
		toggle = false;
	}
	else if (Application::IsKeyPressed('0'))
	{
		toggle = true;
	}
	buttonSleep += dt;
//for lampost, user has to go near before they can trigger with spacebar
	if ((5.73714 < camera.position.x && camera.position.x < 15.424) && (-11.2282 < camera.position.z && camera.position.z < 12.8987) && buttonSleep > 0.5)
	{
		if (Application::IsKeyPressed(VK_SPACE))
		{
			buttonSleep = 0;
			lampostOn = !lampostOn;
		}
	}

	//lightball2 lampost interaction
	if (lampostOn)
		light[1].position.x = 20;
	else
		light[1].position.x = 600;
	camera.Update(dt * 2);
	if (!boneThrown)	//if the bone isn't thrown, it's with the user
		dogBone = camera.position + (2 * camera.view);

	frame = std::to_string(fps);
	//std::cout << dogPos.x << ", " << light[0].position.y << ", " << dogPos.z << std::endl;
	//std::cout << light[0].position.x << ", " << light[0].position.y << ", " << light[0].position.z << std::endl;
	/*std::cout << camera.position.x  << ", " << camera.position.y << ", "  << camera.position.z << ", "
		<<  std::endl;*/
	/*std::cout << "UP Vector values:" << camera.up.x << ", " << camera.up.y << ", " << camera.up.z << ", "
		<< std::endl;*/
	//std::cout << "view vector: " << camera.view.x << ", " << camera.view.y << ", " << camera.view.z 
	//	<< std::endl;
	std::cout << torchlight << std::endl;
	//std::cout << timer0 << std::endl;
}

void slopeMeasureDev::Render()
{

	//Clear color & depth buffer every frame
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	viewStack.LoadIdentity();
	viewStack.LookAt(camera.position.x, camera.position.y, camera.position.z, camera.target.x, camera.target.y, camera.target.z, camera.up.x, camera.up.y, camera.up.z);
	modelStack.LoadIdentity();

	/*modelStack.PushMatrix();
		RenderMesh(meshList[GEO_AXES], false);
	modelStack.PopMatrix();*/

	modelStack.PushMatrix();
		modelStack.Translate(light[0].position.x, light[0].position.y, light[0].position.z);
		modelStack.Scale(0.2, 0.2, 0.2);
		RenderMesh(meshList[GEO_LIGHTBALL], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
		RenderSkyBox();
	modelStack.PopMatrix();

	modelStack.PushMatrix();
		RenderModel();
	modelStack.PopMatrix();


	modelStack.PushMatrix();
		RenderTextOnScreen(meshList[GEO_TEXT], frame, Color(0, 1, 0), 2, 1, 1);
	modelStack.PopMatrix();

	
	if (camera.position.x > -14.8 && camera.position.x < -5.780 &&
		camera.position.z > -14.928 && camera.position.z < 7.259 
		)
	{
		if (doorOpen)
		{
			modelStack.PushMatrix();
			RenderTextOnScreen(meshList[GEO_DOORTEXT], "Press spacebar to close the door", Color(0, 1, 0), 2, 5, 3);
			modelStack.PopMatrix();

			modelStack.PushMatrix();
			RenderTextOnScreen(meshList[GEO_DOORTEXT], "Press e to enter teleportation room", Color(0, 1, 0), 3, 2, 5);
			modelStack.PopMatrix();
			if (Application::IsKeyPressed(VK_SPACE) && buttonSleep > 0.5)
			{
				doorOpen = false;
				buttonSleep = 0;
			}
			else if (Application::IsKeyPressed('E'))
			{
				camera.position.x = 103.387;
				camera.position.z = -102;
			}
		}
		else
		{
			modelStack.PushMatrix();
			RenderTextOnScreen(meshList[GEO_DOORTEXT], "Press spacebar to open the door", Color(0, 1, 0), 2, 5, 3);
			modelStack.PopMatrix();
			if (Application::IsKeyPressed(VK_SPACE) && buttonSleep > 0.5)
			{
				doorOpen = true;
				buttonSleep = 0;
			}
		}
	}

	else if ((5.73714 < camera.position.x && camera.position.x < 15.424) && (-11.2282 < camera.position.z && camera.position.z < 12.8987))
	{
		if (lampostOn)
		{
		modelStack.PushMatrix();
		RenderTextOnScreen(meshList[GEO_DOORTEXT], "Lampost is On", Color(0, 1, 0), 5, 5, 3);
		modelStack.PopMatrix();
		}
		else
		{
			modelStack.PushMatrix();
			RenderTextOnScreen(meshList[GEO_DOORTEXT], "Lampost is Off", Color(0, 1, 0), 5, 5, 3);
			modelStack.PopMatrix();
		}
	}

	if (light[0].type == Light::LIGHT_DIRECTIONAL)
	{
		Vector3 lightDir(light[0].position.x, light[0].position.y, light[0].position.z);
		Vector3 lightDirection_cameraspace = viewStack.Top() * lightDir;
		glUniform3fv(m_parameters[U_LIGHT0_POSITION], 1, &lightDirection_cameraspace.x);
	}
	else if (light[0].type == Light::LIGHT_SPOT)
	{
		Position lightPosition_cameraspace = viewStack.Top() * light[0].position;
		glUniform3fv(m_parameters[U_LIGHT0_POSITION], 1, &lightPosition_cameraspace.x);
		Vector3 spotDirection_cameraspace = viewStack.Top() * light[0].spotDirection;
		glUniform3fv(m_parameters[U_LIGHT0_SPOTDIRECTION], 1, &spotDirection_cameraspace.x);
	}
	else
	{
		Position lightPosition_cameraspace = viewStack.Top() * light[0].position;
		glUniform3fv(m_parameters[U_LIGHT0_POSITION], 1, &lightPosition_cameraspace.x);
	}
	Position lightPosition_cameraspace = viewStack.Top() * light[1].position;
	glUniform3fv(m_parameters[U_LIGHT1_POSITION], 1, &lightPosition_cameraspace.x);
	Vector3 spotDirection_cameraspace = viewStack.Top() * light[1].spotDirection;
	glUniform3fv(m_parameters[U_LIGHT1_SPOTDIRECTION], 1, &spotDirection_cameraspace.x);
}

void slopeMeasureDev::RenderSkyBox()
{
	modelStack.PushMatrix();
		modelStack.Translate(0.0f + camera.position.x, 0.0f, 249.0f + camera.position.z);
		modelStack.Rotate(180, 0, 1, 0);
		modelStack.Scale(500, 500, 1);
		RenderMesh(meshList[GEO_FRONT], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
		modelStack.Translate(0.0f + camera.position.x, 0.0f, -249.0f + camera.position.z);
		modelStack.Scale(500, 500, 1);
		RenderMesh(meshList[GEO_BACK], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
		modelStack.Translate(0.0f + camera.position.x, 249.0f, 0.0f + camera.position.z);
		modelStack.Rotate(180, 0, 1, 0);
		modelStack.Rotate(90, 1, 0, 0);
		modelStack.Rotate(-90, 0, 0, 1);
		modelStack.Scale(500, 500, 1);
		RenderMesh(meshList[GEO_TOP], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
		modelStack.Translate(0.0f + camera.position.x, -249.0f, 0.0f + camera.position.z);
		modelStack.Rotate(180, 0, 1, 0);
		modelStack.Rotate(-90, 1, 0, 0);
		modelStack.Rotate(90, 0, 0, 1);
		modelStack.Scale(500, 500, 1);
		RenderMesh(meshList[GEO_BOTTOM], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
		modelStack.Translate(249.0f + camera.position.x, 0.0f, 0.0f + camera.position.z);
		modelStack.Rotate(-90, 0, 1, 0);
		modelStack.Scale(500, 500, 1);
		RenderMesh(meshList[GEO_LEFT], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
		modelStack.Translate(-249.0f + camera.position.x, 0.0f, 0.0f + camera.position.z);
		modelStack.Rotate(90, 0, 1, 0);
		modelStack.Scale(500, 500, 1);
		RenderMesh(meshList[GEO_RIGHT], false);
	modelStack.PopMatrix();
}

void slopeMeasureDev::RenderModel()
{
	modelStack.PushMatrix();
	modelStack.Scale(10, 10, 10);
	RenderMesh(meshList[GEO_MODEL_LAND], toggle);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(0, 0, -120);
	modelStack.Scale(10, 10, 10);
	RenderMesh(meshList[GEO_MODEL_SLOPE], toggle);
	modelStack.PopMatrix();

	/*modelStack.PushMatrix();
	modelStack.Translate(0, -0.08, -119);
	modelStack.Scale(10, 10, 10);
	RenderMesh(meshList[GEO_MODEL_WALL], toggle);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(0, -0.08, 119);
	modelStack.Rotate(180, 0, 1, 0);
	modelStack.Scale(10, 10, 10);
	RenderMesh(meshList[GEO_MODEL_WALL], toggle);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(119, -0.08, 0);
	modelStack.Rotate(-90, 0, 1, 0);
	modelStack.Scale(10, 10, 10);
	RenderMesh(meshList[GEO_MODEL_WALL], toggle);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(-119, -0.08, 0);
	modelStack.Rotate(90, 0, 1, 0);
	modelStack.Scale(10, 10, 10);
	RenderMesh(meshList[GEO_MODEL_WALL], toggle);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(0, -0.08, 119.52);
	modelStack.Rotate(180, 0, 1, 0);
	modelStack.Scale(10, 10, 10);
	RenderMesh(meshList[GEO_MODEL_WALL], toggle);
	modelStack.PopMatrix();*/

	modelStack.PushMatrix();
	modelStack.Rotate(0, 0, 1, 0);
	modelStack.Translate(-20, 0, 0);
	modelStack.Scale(5, 5, 5);
	RenderMesh(meshList[GEO_MODEL_HOUSE], toggle);
		modelStack.PushMatrix();
		modelStack.Translate(0, 0, 0);
		modelStack.Rotate(timer0, 0, 1, 0);
		modelStack.Scale(1, 1, 1);
		RenderMesh(meshList[GEO_MODEL_DOOR], toggle);
		modelStack.PopMatrix();
	modelStack.PopMatrix();

	//modelStack.PushMatrix();
	//modelStack.Translate(dogPos.x, 10, dogPos.z);
	//modelStack.Rotate(doggyRotate, 0, 1, 0);
	//modelStack.Scale(50, 50, 50);
	//RenderMesh(meshList[GEO_MODEL_DOGBODY], toggle);

	//	modelStack.PushMatrix();	//head
	//	modelStack.Scale(1, 1, 1);
	//	RenderMesh(meshList[GEO_MODEL_DOGHEAD], toggle);
	//	modelStack.PopMatrix();

	//	modelStack.PushMatrix();	//backL
	//	modelStack.Translate(0, 0.09, 0.250);
	//	modelStack.Rotate(dogLeg * -30, 1, 0, 0);
	//	modelStack.Translate(0, -0.09, -0.293);
	//	modelStack.Scale(1, 1, 1);
	//	RenderMesh(meshList[GEO_MODEL_DOGLEG], toggle);
	//	modelStack.PopMatrix();

	//	modelStack.PushMatrix();	//frontL
	//	modelStack.Translate(0, 0.09, -0.250);
	//	modelStack.Rotate(dogLeg * 30, 1, 0, 0);
	//	modelStack.Translate(0, -0.09, -0.293);
	//	modelStack.Scale(1, 1, 1);
	//	RenderMesh(meshList[GEO_MODEL_DOGLEG], toggle);
	//	modelStack.PopMatrix();

	//	modelStack.PushMatrix();	//backR
	//	modelStack.Translate(0.145, 0.09, 0.250);
	//	modelStack.Rotate(dogLeg * 30, 1, 0, 0);
	//	modelStack.Translate(0.145, -0.09, -0.293);
	//	modelStack.Scale(1, 1, 1);
	//	RenderMesh(meshList[GEO_MODEL_DOGLEG], toggle);
	//	modelStack.PopMatrix();

	//	modelStack.PushMatrix();	//frontR
	//	modelStack.Translate(0.145, 0.09, -0.250);
	//	modelStack.Rotate(dogLeg * -30, 1, 0, 0);
	//	modelStack.Translate(0.145, -0.09, -0.293);
	//	modelStack.Scale(1, 1, 1);
	//	RenderMesh(meshList[GEO_MODEL_DOGLEG], toggle);
	//	modelStack.PopMatrix();
	//	
	//modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(20, 0, 0);
	modelStack.Scale(5, 5, 5);
	RenderMesh(meshList[GEO_MODEL_LAMPOST], !lampostOn);
	modelStack.PopMatrix();


	modelStack.PushMatrix();
	modelStack.Translate(15, 0, 0);
	modelStack.Scale(5, 5, 5);
	RenderMesh(meshList[GEO_MODEL_BUTTONBASE], true);
		modelStack.PushMatrix();
		modelStack.Translate(0, buttonTranslate, 0);
		modelStack.Scale(1, 1, 1);
		RenderMesh(meshList[GEO_MODEL_BUTTON], true);
		modelStack.PopMatrix();
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(dogBone.x, dogBone.y, dogBone.z);
	modelStack.Rotate(90, 0, 0, 1);
	modelStack.Scale(0.1, .1, .1);
	RenderMesh(meshList[GEO_MODEL_BONE], true);
	modelStack.PopMatrix();


}

void slopeMeasureDev::RenderMesh(Mesh *mesh, bool enableLight)
{
	Mtx44 MVP, modelView, modelView_inverse_transpose;

	MVP = projectionStack.Top() * viewStack.Top() * modelStack.Top();
	glUniformMatrix4fv(m_parameters[U_MVP], 1, GL_FALSE, &MVP.a[0]);
	modelView = viewStack.Top() * modelStack.Top();
	glUniformMatrix4fv(m_parameters[U_MODELVIEW], 1, GL_FALSE, &modelView.a[0]);

	if (mesh->textureID > 0)
	{
		glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED], 1);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, mesh->textureID);
		glUniform1i(m_parameters[U_COLOR_TEXTURE], 0);
	}
	else
	{
		glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED], 0);
	}

	if (enableLight)
	{
		glUniform1i(m_parameters[U_LIGHTENABLED], 1);
		modelView_inverse_transpose = modelView.GetInverse().GetTranspose();
		glUniformMatrix4fv(m_parameters[U_MODELVIEW_INVERSE_TRANSPOSE], 1, GL_FALSE, &modelView_inverse_transpose.a[0]);

		//load material
		glUniform3fv(m_parameters[U_MATERIAL_AMBIENT], 1, &mesh->material.kAmbient.r);
		glUniform3fv(m_parameters[U_MATERIAL_DIFFUSE], 1, &mesh->material.kDiffuse.r);
		glUniform3fv(m_parameters[U_MATERIAL_SPECULAR], 1, &mesh->material.kSpecular.r);
		glUniform1f(m_parameters[U_MATERIAL_SHININESS], mesh->material.kShininess);
	}
	else
	{
		glUniform1i(m_parameters[U_LIGHTENABLED], 0);
	}

	mesh->Render();

	if (mesh->textureID > 0)
	{
		glBindTexture(GL_TEXTURE_2D, 0);
	}
}

void slopeMeasureDev::Exit()
{

	for (int i = 0; i < NUM_GEOMETRY; ++i)
	{
		if (meshList[i] != NULL)
		{
			delete meshList[i];
		}
	}

	// Cleanup VBO here
	glDeleteVertexArrays(1, &m_vertexArrayID);
	glDeleteProgram(m_programID);
}

void slopeMeasureDev::RenderText(Mesh* mesh, std::string text, Color color)
{
	if (!mesh || mesh->textureID <= 0) //Proper error check
	{
		return;
	}

	glDisable(GL_DEPTH_TEST);
	glUniform1i(m_parameters[U_TEXT_ENABLED], 1);
	glUniform3fv(m_parameters[U_TEXT_COLOR], 1, &color.r);
	glUniform1i(m_parameters[U_LIGHTENABLED], 0);
	glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED], 1);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, mesh->textureID);
	glUniform1i(m_parameters[U_COLOR_TEXTURE], 0);
	for (unsigned i = 0; i < text.length(); ++i)
	{
		Mtx44 characterSpacing;
		characterSpacing.SetToTranslation(i * 1.0f, 0, 0); //1.0f is the spacing of each character, you may change this value
		Mtx44 MVP = projectionStack.Top() * viewStack.Top() * modelStack.Top() * characterSpacing;
		glUniformMatrix4fv(m_parameters[U_MVP], 1, GL_FALSE, &MVP.a[0]);

		mesh->Render((unsigned)text[i] * 6, 6);
	}
	glBindTexture(GL_TEXTURE_2D, 0);
	glUniform1i(m_parameters[U_TEXT_ENABLED], 0);
	glEnable(GL_DEPTH_TEST);
}

void slopeMeasureDev::RenderTextOnScreen(Mesh* mesh, std::string text, Color color, float size, float x, float y)
{
	if (!mesh || mesh->textureID <= 0) //Proper error check
	{
		return;
	}

	glDisable(GL_DEPTH_TEST);

	Mtx44 ortho;
	ortho.SetToOrtho(0, 80, 0, 60, -10, 10); //size of screen UI
	projectionStack.PushMatrix();
	projectionStack.LoadMatrix(ortho);
	viewStack.PushMatrix();
	viewStack.LoadIdentity(); //No need camera for ortho mode
	modelStack.PushMatrix();
	modelStack.LoadIdentity(); //Reset modelStack
	modelStack.Scale(size, size, size);
	modelStack.Translate(x, y, 0);

	glUniform1i(m_parameters[U_TEXT_ENABLED], 1);
	glUniform3fv(m_parameters[U_TEXT_COLOR], 1, &color.r);
	glUniform1i(m_parameters[U_LIGHTENABLED], 0);
	glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED], 1);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, mesh->textureID);
	glUniform1i(m_parameters[U_COLOR_TEXTURE], 0);
	for (unsigned i = 0; i < text.length(); ++i)
	{
		Mtx44 characterSpacing;
		characterSpacing.SetToTranslation(i * .7f, 0, 0); //1.0f is the spacing of each character, you may change this value
		Mtx44 MVP = projectionStack.Top() * viewStack.Top() * modelStack.Top() * characterSpacing;
		glUniformMatrix4fv(m_parameters[U_MVP], 1, GL_FALSE, &MVP.a[0]);

		mesh->Render((unsigned)text[i] * 6, 6);
	}
	glBindTexture(GL_TEXTURE_2D, 0);
	glUniform1i(m_parameters[U_TEXT_ENABLED], 0);

	projectionStack.PopMatrix();
	viewStack.PopMatrix();
	modelStack.PopMatrix();

	glEnable(GL_DEPTH_TEST);
}
