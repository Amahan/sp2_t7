#ifndef SLOPE_MEASURE_DEV_H
#define SLOPE_MEASURE_DEV_H

#include "Scene.h"
#include "Camera.h"
#include "Mesh.h"
#include "MeshBuilder.h"
#include "MatrixStack.h"
#include "Light.h"
#include "slopeCam.h"
#include "string"

class slopeMeasureDev : public Scene
{
	enum UNIFORM_TYPE
	{
		U_MVP = 0,
		U_MODELVIEW,
		U_MODELVIEW_INVERSE_TRANSPOSE,
		U_MATERIAL_AMBIENT,
		U_MATERIAL_DIFFUSE,
		U_MATERIAL_SPECULAR,
		U_MATERIAL_SHININESS,
		U_LIGHTENABLED,

		U_LIGHT0_POSITION,
		U_LIGHT0_COLOR,
		U_LIGHT0_POWER,
		U_LIGHT0_KC,
		U_LIGHT0_KL,
		U_LIGHT0_KQ,
		U_LIGHT0_TYPE,
		U_LIGHT0_SPOTDIRECTION,
		U_LIGHT0_COSCUTOFF,
		U_LIGHT0_COSINNER,
		U_LIGHT0_EXPONENT,

		U_LIGHT1_POSITION,
		U_LIGHT1_COLOR,
		U_LIGHT1_POWER,
		U_LIGHT1_KC,
		U_LIGHT1_KL,
		U_LIGHT1_KQ,
		U_LIGHT1_TYPE,
		U_LIGHT1_SPOTDIRECTION,
		U_LIGHT1_COSCUTOFF,
		U_LIGHT1_COSINNER,
		U_LIGHT1_EXPONENT,

		U_LIGHT2_POSITION,
		U_LIGHT2_COLOR,
		U_LIGHT2_POWER,
		U_LIGHT2_KC,
		U_LIGHT2_KL,
		U_LIGHT2_KQ,
		U_LIGHT2_TYPE,
		U_LIGHT2_SPOTDIRECTION,
		U_LIGHT2_COSCUTOFF,
		U_LIGHT2_COSINNER,
		U_LIGHT2_EXPONENT,

		U_NUMLIGHTS,

		U_COLOR_TEXTURE_ENABLED,
		U_COLOR_TEXTURE,

		U_TEXT_ENABLED,
		U_TEXT_COLOR,

		U_TOTAL,
	};

	enum GEOMETRY_TYPE
	{
		GEO_AXES,
		GEO_QUAD,
		GEO_CUBE,
		GEO_CIRCLE,
		GEO_RING,
		GEO_SPHERE,
		GEO_LIGHTBALL,
		GEO_LIGHTBALL1,
		GEO_LEFT,
		GEO_RIGHT,
		GEO_TOP,
		GEO_BOTTOM,
		GEO_FRONT,
		GEO_BACK,
		GEO_MODEL1,
		GEO_MODEL_LAND,
		GEO_MODEL_DOOR,
		GEO_MODEL_HOUSE,
		GEO_MODEL_DOGBODY,
		GEO_MODEL_DOGLEG,
		GEO_MODEL_DOGHEAD,
		GEO_MODEL_DOGLEFTEAR,
		GEO_MODEL_DOGRIGHTEAR,
		GEO_MODEL_DOGTAIL,
		GEO_MODEL_CHAIR,
		GEO_MODEL_DART,
		GEO_MODEL_DARTBOARD,
		GEO_MODEL_DOORMAN,
		GEO_MODEL_SHOE,
		GEO_MODEL_WINEBOTTLE,
		GEO_MODEL_LAMPOST,
		GEO_MODEL_BUTTONBASE,
		GEO_MODEL_BUTTON,
		GEO_MODEL_BONE,
		GEO_MODEL_WALL,
		GEO_MODEL_SLOPE,
		GEO_TEXT,
		GEO_LAMPTEXT,
		GEO_DOORTEXT,
		NUM_GEOMETRY,
	};

public:
	slopeMeasureDev();
	~slopeMeasureDev();

	//here's the time variables for my animated objects
	float timer0;
	bool doorOpen;
	float dogLeg;
	float doggyRotate;
	float goalAngle;
	float buttonTranslate;
	float buttonSleep;
	//here's the bool variables that change how time variables work
	bool timer0Rev;
	bool dogLegRev;
	bool dogMove;
	bool lampostOn;
	bool boneThrown;
	virtual void Init();
	virtual void Update(double dt);
	virtual void Render();
	virtual void Exit();
	
	Vector3 teleport;
	Vector3 dogBoneDir;
	Vector3 dogBone;
	Vector3 dogGoalView;
	Vector3 dogPos;
	Vector3 dogRight;
	Vector3 dogUp;
	Vector3 dogTarget;
	Vector3 dogView;
private:
	unsigned m_vertexArrayID;
	Mesh* meshList[NUM_GEOMETRY];
	unsigned m_programID;
	unsigned m_parameters[U_TOTAL];
	bool toggle;
	std::string frame;
	float fps;
	int torchlight;
	bool revTorch;

	//Camera camera;
	SlopeCam camera;
	MS modelStack, viewStack, projectionStack;
	Light light[2];

	void RenderMesh(Mesh *mesh, bool enableLight);
	void RenderSkyBox();
	void RenderModel();
	void RenderText(Mesh* mesh, std::string text, Color color);
	void RenderTextOnScreen(Mesh* mesh, std::string text, Color color, float size, float x, float y);
};

#endif