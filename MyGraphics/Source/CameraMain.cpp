#include "CameraMain.h"
#include "Application.h"
#include "Mtx44.h"

CameraMain::CameraMain()
{
	xmousepos = 0;
	ymousepos = 0;
	player_Stamina = 100.0;
}

CameraMain::~CameraMain()
{
}

void CameraMain::Init(const Vector3& pos, const Vector3& target, const Vector3& up)
{
	this->position = defaultPosition = pos;
	this->target = defaultTarget = target;
	this->view = (target - position).Normalized();
	this->right = view.Cross(up);
	right.y = 0;
	right.Normalize();
	this->up = defaultUp = right.Cross(view).Normalized();

	position.y = -17;

	Xyaw = 3.14;
    Ypitch = 0;
}

void CameraMain::Update(double dt)
{
	Vector3 view = target - position;
	Vector3 right = view.Cross(up);
	Mtx44 rotation;

	static const float CAMERA_SPEED = 50.f;
	static const float MOVEMENT_SPEED = 0.5f;

	if (Application::IsKeyPressed(VK_SHIFT) == false && player_Stamina < 100)
	{
		player_Stamina += 0.50;
	}

	if(Application::IsKeyPressed('A'))
	{
		position = position - right * MOVEMENT_SPEED;
		
		position.y = -17;

		target = position + view;
		
	}
	if(Application::IsKeyPressed('D'))
	{
		
		position = position + right * MOVEMENT_SPEED;
		position.y = -17;
		target = position + view;
		
	}
	if(Application::IsKeyPressed('W'))
	{
		
		position = position + view * MOVEMENT_SPEED;
		position.y = -17;
		target = position + view;
		if (Application::IsKeyPressed(VK_SHIFT) && player_Stamina >= 20) // Only able to sprint if moving forwards
		{
			position = position + view * (MOVEMENT_SPEED + 0.1);
			position.y = -17;
			target = position + view;
			player_Stamina -= 5;
		}
		
	} 
	if(Application::IsKeyPressed('S'))
	{
		position = position - view * MOVEMENT_SPEED;
		position.y = -17;
		target = position + view;
		
	}

	

	if (Application::IsKeyPressed('N'))
	{
		if (Application::IsKeyPressed(VK_CONTROL))
			position.y += up.y * (float)(0.02f * dt);
		else
			position.y += view.y * (float)(30.f * dt);
		target = position + view;
		/*Vector3 direction = target - position;
		if (direction.Length() > 5)
		{
		Vector3 view = (target - position).Normalized();
		position += view * (float)(10.f * dt);
		}*/
	}
	if (Application::IsKeyPressed('M'))
	{

		if (Application::IsKeyPressed(VK_CONTROL))
			position.y -= up.y * (float)(0.02f * dt);
		else
			position.y -= view.y * (float)(30.f * dt);
		target = position + view;
		/*Vector3 view = (target - position).Normalized();
		position -= view * (float)(10.f * dt);*/
	}

	if (Application::IsKeyPressed(VK_LSHIFT))
	{
		//Reset();
		view.z = 1;
		view.x = 0;
		view.y = 0;
		view.Normalize();
		target = position + view;
	}
	/*if(Application::IsKeyPressed('N'))
	{
		Vector3 direction = target - position;
		if(direction.Length() > 5)
		{
			Vector3 view = (target - position).Normalized();
			position += view * (float)(10.f * dt);
		}
	}
	if(Application::IsKeyPressed('M'))
	{
		Vector3 view = (target - position).Normalized();
		position -= view * (float)(10.f * dt);
	}*/
/*	if (Application::IsKeyPressed(VK_RIGHT))
	{
		float yaw = (float)(-CAMERA_SPEED * dt);
		Vector3 view = (target - position).Normalized();
		Vector3 right = view.Cross(up);
		right.y = 0;
		right.Normalize();
		up = right.Cross(view).Normalized();
		Mtx44 rotation;
		rotation.SetToRotation(yaw, up.x, up.y, up.z);

		view = rotation * view;
		target = position + view;
	}
	if (Application::IsKeyPressed(VK_LEFT))
	{
		float yaw = (float)(CAMERA_SPEED * dt);
		Vector3 view = (target - position).Normalized();
		Vector3 right = view.Cross(up);
		right.y = 0;
		right.Normalize();
		up = right.Cross(view).Normalized();
		Mtx44 rotation;
		rotation.SetToRotation(yaw, up.x, up.y, up.z);

		view = rotation * view;
		target = position + view;
	}
	if (Application::IsKeyPressed(VK_UP))
	{
		float pitch = (float)(CAMERA_SPEED * dt);
		Vector3 view = (target - position).Normalized();
		right.y = 0;
		right.Normalize();
		up = right.Cross(view).Normalized();
		
		rotation.SetToRotation(pitch, right.x, right.y, right.z);
		view = rotation * view;
		target = position + view;
	}
	if (Application::IsKeyPressed(VK_DOWN))
	{
		float pitch = (float)(-CAMERA_SPEED * dt);
		Vector3 view = (target - position).Normalized();
		right.y = 0;
		right.Normalize();
		up = right.Cross(view).Normalized();
		
		rotation.SetToRotation(pitch, right.x, right.y, right.z);
		view = rotation * view;
		target = position + view;
	} */

	//FOR FINDING NORMALS

	if (Application::IsKeyPressed(VK_LEFT))
	{
		float yaw = (float)(CAMERA_SPEED * dt);
		Mtx44 rotation;

		view = (target - position).Normalized();
		right = view.Cross(up);
		right.y = 0;
		right.Normalize();
		up = right.Cross(view).Normalized();

		rotation.SetToRotation(yaw, 0, 1, 0);
		view = rotation * view;
		target = position + view;
	}
	if (Application::IsKeyPressed(VK_RIGHT))
	{
		float yaw = (float)(-CAMERA_SPEED * dt);
		Mtx44 rotation;

		view = (target - position).Normalized();
		right = view.Cross(up);
		right.y = 0;
		right.Normalize();
		up = right.Cross(view).Normalized();

		rotation.SetToRotation(yaw, 0, 1, 0);
		view = rotation * view;
		target = position + view;
	}
	if (Application::IsKeyPressed(VK_UP))
	{
		if (Application::IsKeyPressed(VK_RSHIFT))
		{
			float pitch = (float)(0.02f * dt);
			Mtx44 rotation;
			right.y = 0;
			right.Normalize();
			up = right.Cross(view).Normalized();
			rotation.SetToRotation(pitch, right.x, right.y, right.z);
			view = rotation * view;
			target = position + view;
		}
		else
		{
			float pitch = (float)(CAMERA_SPEED * dt);
			Mtx44 rotation;
			right.y = 0;
			right.Normalize();
			up = right.Cross(view).Normalized();
			rotation.SetToRotation(pitch, right.x, right.y, right.z);
			view = rotation * view;
			target = position + view;
		}
	}
	if (Application::IsKeyPressed(VK_DOWN))
	{
		if (Application::IsKeyPressed(VK_RSHIFT))
		{
			float pitch = (float)(-0.02f * dt);
			Mtx44 rotation;
			right.y = 0;
			right.Normalize();
			up = right.Cross(view).Normalized();
			rotation.SetToRotation(pitch, right.x, right.y, right.z);
			view = rotation * view;
			target = position + view;
		}
		else
		{
			float pitch = (float)(-CAMERA_SPEED * dt);
			Mtx44 rotation;
			right.y = 0;
			right.Normalize();
			up = right.Cross(view).Normalized();
			rotation.SetToRotation(pitch, right.x, right.y, right.z);
			view = rotation * view;
			target = position + view;
		}
	}

	Application::GetMousePosition(xmousepos, ymousepos);
	Xyaw += (dt * 2 * float(800 / 2 - xmousepos));
	Ypitch += (dt * 2 * float(600 / 2 - ymousepos));

	yaw = Xyaw;
	pitch = Ypitch;

	view = (target - position).Normalized();
	right = view.Cross(up);
	right.y = 0;
	right.Normalize();
	up = right.Cross(view).Normalized();

	Mtx44 rotation2;
	rotation.SetToRotation(pitch, right.x, right.y, right.z);
	rotation2.SetToRotation(yaw, up.x, up.y, up.z);

	view = rotation * rotation2 * view;
	target = position + view;

	Application::SetMousePosition();
	Xyaw = 0;
	Ypitch = 0;

	if(Application::IsKeyPressed('R'))
	{
		Reset();
	}
}


void CameraMain::Reset()
{
	position = defaultPosition;
	target = defaultTarget;
	up = defaultUp;
}
