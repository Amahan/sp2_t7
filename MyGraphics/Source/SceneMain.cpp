#include "SceneMain.h"
#include "GL\glew.h"
#include <string>

#include "shader.hpp"
#include "Mtx44.h"
#include "Application.h"
#include "Utility.h"
#include "LoadTGA.h"
#include "Box.h"



SceneMain::SceneMain()
{
}

SceneMain::~SceneMain()
{
}

void SceneMain::Init()
{
	tankCamera.position.x = 20;
	tankCamera.position.y = -20;
	tankCamera.position.z = 18;
	gunCamera.position.x = camera.position.x - 0.33;
	gunCamera.position.y = camera.position.y - 2;
	gunCamera.position.z = camera.position.z + 1;
	

	player_IsSprint = false;
	player_Stamina = 100;

	tankyaw = 0;
	
	activetext.x = -3;
	activetext.y = -10;
	activetext1.x = -3;
	activetext1.y = -15;
	activetext.z = 0;
	activetext1.z = 0;
	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

	player_RideState = NOT_RIDE;
	player_CurrentWep = HOLD_NOTHING;

	// Generate a default VAO for now
	glGenVertexArrays(1, &m_vertexArrayID);
	glBindVertexArray(m_vertexArrayID);
	glEnable(GL_CULL_FACE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	camera.Init(Vector3(0, -17, -40), Vector3(0, -17, 0), Vector3(0, 1, 0));
	gunCamera.Init(Vector3(camera.position.x - 0.33 + camera.view.x, camera.position.y - 2, camera.position.z + 1), Vector3(camera.target.x, camera.target.y, camera.target.z), Vector3(camera.up.x, camera.up.y, camera.up.z));

	Mtx44 projection;
	projection.SetToPerspective(45.f, 4.f / 3.f, 0.1f, 1000.f);
	projectionStack.LoadMatrix(projection);

	m_programID = LoadShaders("Shader//Texture.vertexshader", "Shader//Text.fragmentshader");

	// Get a handle for our "colorTexture" uniform
	m_parameters[U_COLOR_TEXTURE_ENABLED] = glGetUniformLocation(m_programID, "colorTextureEnabled");
	m_parameters[U_COLOR_TEXTURE] = glGetUniformLocation(m_programID, "colorTexture");


	m_parameters[U_MVP] = glGetUniformLocation(m_programID, "MVP");
	m_parameters[U_MODELVIEW] = glGetUniformLocation(m_programID, "MV");
	m_parameters[U_MODELVIEW_INVERSE_TRANSPOSE] = glGetUniformLocation(m_programID, "MV_inverse_transpose");
	m_parameters[U_MATERIAL_AMBIENT] = glGetUniformLocation(m_programID, "material.kAmbient");
	m_parameters[U_MATERIAL_DIFFUSE] = glGetUniformLocation(m_programID, "material.kDiffuse");
	m_parameters[U_MATERIAL_SPECULAR] = glGetUniformLocation(m_programID, "material.kSpecular");
	m_parameters[U_MATERIAL_SHININESS] = glGetUniformLocation(m_programID, "material.kShininess");
	m_parameters[U_LIGHT0_POSITION] = glGetUniformLocation(m_programID, "lights[0].position_cameraspace");
	m_parameters[U_LIGHT0_COLOR] = glGetUniformLocation(m_programID, "lights[0].color");
	m_parameters[U_LIGHT0_POWER] = glGetUniformLocation(m_programID, "lights[0].power");
	m_parameters[U_LIGHT0_KC] = glGetUniformLocation(m_programID, "lights[0].kC");
	m_parameters[U_LIGHT0_KL] = glGetUniformLocation(m_programID, "lights[0].kL");
	m_parameters[U_LIGHT0_KQ] = glGetUniformLocation(m_programID, "lights[0].kQ");
	m_parameters[U_LIGHT1_POSITION] = glGetUniformLocation(m_programID, "lights[1].position_cameraspace");
	m_parameters[U_LIGHT1_COLOR] = glGetUniformLocation(m_programID, "lights[1].color");
	m_parameters[U_LIGHT1_POWER] = glGetUniformLocation(m_programID, "lights[1].power");
	m_parameters[U_LIGHT1_KC] = glGetUniformLocation(m_programID, "lights[1].kC");
	m_parameters[U_LIGHT1_KL] = glGetUniformLocation(m_programID, "lights[1].kL");
	m_parameters[U_LIGHT1_KQ] = glGetUniformLocation(m_programID, "lights[1].kQ");
	m_parameters[U_LIGHTENABLED] = glGetUniformLocation(m_programID, "lightEnabled");
	m_parameters[U_TEXT_ENABLED] = glGetUniformLocation(m_programID, "textEnabled");
	m_parameters[U_TEXT_COLOR] = glGetUniformLocation(m_programID, "textColor");



	m_parameters[U_NUMLIGHTS] = glGetUniformLocation(m_programID, "numLights");
	m_parameters[U_LIGHT0_TYPE] = glGetUniformLocation(m_programID, "lights[0].type");
	m_parameters[U_LIGHT0_SPOTDIRECTION] = glGetUniformLocation(m_programID, "lights[0].spotDirection");
	m_parameters[U_LIGHT0_COSCUTOFF] = glGetUniformLocation(m_programID, "lights[0].cosCutoff");
	m_parameters[U_LIGHT0_COSINNER] = glGetUniformLocation(m_programID, "lights[0].cosInner");
	m_parameters[U_LIGHT0_EXPONENT] = glGetUniformLocation(m_programID, "lights[0].exponent");
	m_parameters[U_LIGHT1_TYPE] = glGetUniformLocation(m_programID, "lights[1].type");
	m_parameters[U_LIGHT1_SPOTDIRECTION] = glGetUniformLocation(m_programID, "lights[1].spotDirection");
	m_parameters[U_LIGHT1_COSCUTOFF] = glGetUniformLocation(m_programID, "lights[1].cosCutoff");
	m_parameters[U_LIGHT1_COSINNER] = glGetUniformLocation(m_programID, "lights[1].cosInner");
	m_parameters[U_LIGHT1_EXPONENT] = glGetUniformLocation(m_programID, "lights[1].exponent");


	m_parameters[U_MVP] = glGetUniformLocation(m_programID, "MVP");

	glUseProgram(m_programID);

	glUniform1i(m_parameters[U_NUMLIGHTS], 3);

	// Enable depth test
	glEnable(GL_DEPTH_TEST);

	light[0].type = Light::LIGHT_DIRECTIONAL;
	light[0].position.Set(0, 40, 60);
	light[0].color.Set(1, 1, 1);
	light[0].power = 0.9;
	light[0].kC = 1.f;
	light[0].kL = 0.01f;
	light[0].kQ = 0.001f;
	light[0].cosCutoff = cos(Math::DegreeToRadian(45));
	light[0].cosInner = cos(Math::DegreeToRadian(30));
	light[0].exponent = 3.f;
	light[0].spotDirection.Set(20.f, 0.f, 20.f);

	light[1].type = Light::LIGHT_SPOT;
	light[1].position.Set(0, 0, 60);
	light[1].color.Set(0.5, 1, 0.5);
	light[1].power = 1;
	light[1].kC = 1.f;
	light[1].kL = 0.01f;
	light[1].kQ = 0.001f;
	light[1].cosCutoff = cos(Math::DegreeToRadian(45));
	light[1].cosInner = cos(Math::DegreeToRadian(30));
	light[1].exponent = 3.f;
	light[1].spotDirection.Set(0.f, 0.f, 0.f);

	glUniform1i(m_parameters[U_LIGHT0_TYPE], light[0].type);
	glUniform3fv(m_parameters[U_LIGHT0_COLOR], 1, &light[0].color.r);
	glUniform1f(m_parameters[U_LIGHT0_POWER], light[0].power);
	glUniform1f(m_parameters[U_LIGHT0_KC], light[0].kC);
	glUniform1f(m_parameters[U_LIGHT0_KL], light[0].kL);
	glUniform1f(m_parameters[U_LIGHT0_KQ], light[0].kQ);
	glUniform1f(m_parameters[U_LIGHT0_COSCUTOFF], light[0].cosCutoff);
	glUniform1f(m_parameters[U_LIGHT0_COSINNER], light[0].cosInner);
	glUniform1f(m_parameters[U_LIGHT0_EXPONENT], light[0].exponent);

	glUniform1i(m_parameters[U_LIGHT1_TYPE], light[1].type);
	glUniform3fv(m_parameters[U_LIGHT1_COLOR], 1, &light[1].color.r);
	glUniform1f(m_parameters[U_LIGHT1_POWER], light[1].power);
	glUniform1f(m_parameters[U_LIGHT1_KC], light[1].kC);
	glUniform1f(m_parameters[U_LIGHT1_KL], light[1].kL);
	glUniform1f(m_parameters[U_LIGHT1_KQ], light[1].kQ);
	glUniform1f(m_parameters[U_LIGHT1_COSCUTOFF], light[1].cosCutoff);
	glUniform1f(m_parameters[U_LIGHT1_COSINNER], light[1].cosInner);
	glUniform1f(m_parameters[U_LIGHT1_EXPONENT], light[1].exponent);




	for (int i = 0; i < NUM_GEOMETRY; ++i)
	{
		meshList[i] = NULL;
	}

	meshList[GEO_AXES] = MeshBuilder::GenerateAxes("reference", 1000, 1000, 1000);
	meshList[GEO_SPHERE1] = MeshBuilder::GenerateSphere("sphere", Color(1, 0, 0), 18, 36, 1);

	meshList[GEO_LIGHTBALL] = MeshBuilder::GenerateSphere("sphere", Color(1, 1, 1), 18, 36, 50);
	meshList[GEO_LIGHT1] = MeshBuilder::GenerateSphere("sphere", Color(0.5, 1, 0.5), 18, 36, 500);

	meshList[GEO_QUAD] = MeshBuilder::GenerateQuad("quad", Color(1, 1, 1), 1.f, 1.f);
	meshList[GEO_QUAD]->textureID = LoadTGA("Image//color2.tga");

	meshList[GEO_LEFT] = MeshBuilder::GenerateQuad("left", Color(1, 1, 1), 1.0f, 1.0f);
	meshList[GEO_LEFT]->textureID = LoadTGA("Image//hangingstone_rt.tga");

	meshList[GEO_RIGHT] = MeshBuilder::GenerateQuad("right", Color(1, 1, 1), 1.0f, 1.0f);
	meshList[GEO_RIGHT]->textureID = LoadTGA("Image//hangingstone_lf.tga");

	meshList[GEO_TOP] = MeshBuilder::GenerateQuad("top", Color(1, 1, 1), 1.0f, 1.0f);
	meshList[GEO_TOP]->textureID = LoadTGA("Image//hangingstone_up.tga");

	meshList[GEO_BOTTOM] = MeshBuilder::GenerateQuad("bottom", Color(1, 1, 1), 1.0f, 1.0f);
	meshList[GEO_BOTTOM]->textureID = LoadTGA("Image//hangingstone_dn.tga");

	meshList[GEO_FRONT] = MeshBuilder::GenerateQuad("front", Color(1, 1, 1), 1.0f, 1.0f);
	meshList[GEO_FRONT]->textureID = LoadTGA("Image//hangingstone_ft.tga");

	meshList[GEO_BACK] = MeshBuilder::GenerateQuad("back", Color(1, 1, 1), 1.0f, 1.0f);
	meshList[GEO_BACK]->textureID = LoadTGA("Image//hangingstone_bk.tga");

	meshList[GEO_MODEL1] = MeshBuilder::GenerateOBJ("ground", "OBJ//snowground.obj");
	meshList[GEO_MODEL1]->textureID = LoadTGA("Image//snowflowtexture.tga");

	meshList[GEO_MODEL2] = MeshBuilder::GenerateOBJ("slopes", "OBJ//snowslope.obj");
	meshList[GEO_MODEL2]->textureID = LoadTGA("Image//snowflowtexture.tga");

	meshList[GEO_FPS] = MeshBuilder::GenerateText("UI", 16, 16);
	meshList[GEO_FPS]->textureID = LoadTGA("Image//calibri.tga");

	meshList[GEO_FPS] = MeshBuilder::GenerateText("UI", 16, 16);
	meshList[GEO_FPS]->textureID = LoadTGA("Image//calibri.tga");



}

bool isPointinBox(Vector3 position, Box box)
{
	return (position.x >= box.minX && position.x <= box.maxX) &&
		(position.y >= box.minY && position.y <= box.maxY) &&
		(position.z >= box.minZ && position.z <= box.maxZ);
}

bool isPointoutBox(Vector3 position, Box box)
{
	if ((position.x >= box.minX && position.x <= box.maxX) &&
		(position.y >= box.minY && position.y <= box.maxY) &&
		(position.z >= box.minZ && position.z <= box.maxZ))
	{
		return false;
	}
	else
		return true;
}

void SceneMain::BoundsCheck(double dt)
{
	Vector3 view = camera.target - camera.position;
	Vector3 right = view.Cross(camera.up);


	Box skybox = Box(Vector3(0, 0, 0), 98);


	static const float CAMERA_SPEED = 50.f;
	static const float MOVEMENT_SPEED = 0.5f;


	bool collided = isPointinBox(camera.position, skybox);

	CollisionFunction(collided);

}

void SceneMain::CollisionFunction(bool check)
{

	Vector3 view = camera.target - camera.position;
	Vector3 right = view.Cross(camera.up);

	static const float CAMERA_SPEED = 50.f;
	static const float MOVEMENT_SPEED = 0.5f;

	if (check)
	{
		//std::cout << camera.position << std::endl;
		//std::cout << "Collison";
	}
	else if (Application::IsKeyPressed('D'))
	{
		camera.position = camera.position - right * MOVEMENT_SPEED;
		camera.target = camera.position + view;

		if (Application::IsKeyPressed('W'))
		{
			camera.position = camera.position - view * MOVEMENT_SPEED;
			camera.target = camera.position + view;
			if (Application::IsKeyPressed(VK_SHIFT)) 
			{
				camera.position = camera.position - view * (MOVEMENT_SPEED + 0.1);
				camera.target = camera.position + view;
			}
		}

		if (Application::IsKeyPressed('S'))
		{
			camera.position = camera.position + view * MOVEMENT_SPEED;
			camera.target = camera.position + view;
		}

		else if (Application::IsKeyPressed('A'))
		{
			camera.position = camera.position + right * MOVEMENT_SPEED;
			camera.target = camera.position + view;
		}
	}
	else if (Application::IsKeyPressed('W'))
	{
		camera.position = camera.position - view * MOVEMENT_SPEED;
		camera.target = camera.position + view;
		if (Application::IsKeyPressed(VK_SHIFT))
		{
			camera.position = camera.position - view * (MOVEMENT_SPEED + 0.1);
			camera.target = camera.position + view;
		}

		if (Application::IsKeyPressed('D'))
		{
			camera.position = camera.position - right * MOVEMENT_SPEED;
			camera.target = camera.position + view;
		}

		if (Application::IsKeyPressed('S'))
		{
			camera.position = camera.position + view * MOVEMENT_SPEED;
			camera.target = camera.position + view;
		}

		else if (Application::IsKeyPressed('A'))
		{
			camera.position = camera.position + right * MOVEMENT_SPEED;
			camera.target = camera.position + view;
		}
	}
	else if (Application::IsKeyPressed('A'))
	{
		camera.position = camera.position + right * MOVEMENT_SPEED;
		camera.target = camera.position + view;

		if (Application::IsKeyPressed('D'))
		{
			camera.position = camera.position - right * MOVEMENT_SPEED;
			camera.target = camera.position + view;
		}

		if (Application::IsKeyPressed('S'))
		{
			camera.position = camera.position + view * MOVEMENT_SPEED;
			camera.target = camera.position + view;
		}

		else if (Application::IsKeyPressed('W'))
		{
			camera.position = camera.position - view * MOVEMENT_SPEED;
			camera.target = camera.position + view;
			if (Application::IsKeyPressed(VK_SHIFT))
			{
				camera.position = camera.position - view * (MOVEMENT_SPEED + 0.1);
				camera.target = camera.position + view;
			}
		}
	}
	else if (Application::IsKeyPressed('S'))
	{
		camera.position = camera.position + view * MOVEMENT_SPEED;
		camera.target = camera.position + view;

		if (Application::IsKeyPressed('D'))
		{
			camera.position = camera.position - right * MOVEMENT_SPEED;
			camera.target = camera.position + view;
		}

		if (Application::IsKeyPressed('A'))
		{
			camera.position = camera.position + right * MOVEMENT_SPEED;
			camera.target = camera.position + view;
		}

		else if (Application::IsKeyPressed('W'))
		{
			camera.position = camera.position - view * MOVEMENT_SPEED;
			camera.target = camera.position + view;
			if (Application::IsKeyPressed(VK_SHIFT))
			{
				camera.position = camera.position - view * (MOVEMENT_SPEED + 0.1);
				camera.target = camera.position + view;
			}
		}
	}
}


void SceneMain::Update(double dt)
{
	static const float CAMERA_SPEED = 50.f;
	static const float MOVEMENT_SPEED = 0.5f;
	static const float LSPEED = 10.0f;

	if (Application::IsKeyPressed('1'))
	{
		glEnable(GL_CULL_FACE);
	}
	if (Application::IsKeyPressed('2'))
	{
		glDisable(GL_CULL_FACE);
	}
	if (Application::IsKeyPressed('3'))
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}
	if (Application::IsKeyPressed('4'))
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}

	if (Application::IsKeyPressed('I'))
		light[0].position.z -= (float)(LSPEED * dt);
	if (Application::IsKeyPressed('K'))
		light[0].position.z += (float)(LSPEED * dt);
	if (Application::IsKeyPressed('J'))
		light[0].position.x -= (float)(LSPEED * dt);
	if (Application::IsKeyPressed('L'))
		light[0].position.x += (float)(LSPEED * dt);
	if (Application::IsKeyPressed('O'))
		light[0].position.y -= (float)(LSPEED * dt);
	if (Application::IsKeyPressed('P'))
		light[0].position.y += (float)(LSPEED * dt);

	camera.Update(dt);


	std::cout << camera.position.x  << ", " << camera.position.y << ", "  << camera.position.z << ", "
	<<  std::endl;
	std::cout << "UP Vector values:" << camera.up.x << ", " << camera.up.y << ", " << camera.up.z << ", "
	<< std::endl;
	std::cout << "view vector: " << camera.view.x << ", " << camera.view.y << ", " << camera.view.z	<< std::endl;
	BoundsCheck(dt);
}


void SceneMain::gunCoordSet(Position temp, float x, float y, float z) 
{
	temp.x = x;
	temp.y = y;
	temp.z = z;
}

void SceneMain::Render()
{
	//Clear color & depth buffer every frame
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	
	viewStack.LoadIdentity();
	viewStack.LookAt(camera.position.x, camera.position.y, camera.position.z, camera.target.x, camera.target.y, camera.target.z, camera.up.x, camera.up.y, camera.up.z);
	modelStack.LoadIdentity();

	//Position lightPosition_cameraspace = viewStack.Top() * light[0].position;
	//glUniform3fv(m_parameters[U_LIGHT0_POSITION], 1, &lightPosition_cameraspace.x);
	if (light[0].type == Light::LIGHT_DIRECTIONAL)
	{
		Vector3 lightDir(light[0].position.x, light[0].position.y, light[0].position.z);
		Vector3 lightDirection_cameraspace = viewStack.Top() * lightDir;
		glUniform3fv(m_parameters[U_LIGHT0_POSITION], 1, &lightDirection_cameraspace.x);
	}

	if (light[1].type == Light::LIGHT_SPOT)
	{
		Position lightPosition_cameraspace = viewStack.Top() * light[1].position;
		glUniform3fv(m_parameters[U_LIGHT1_POSITION], 1, &lightPosition_cameraspace.x);
		Vector3 spotDirection_cameraspace = viewStack.Top() * light[1].spotDirection;
		glUniform3fv(m_parameters[U_LIGHT1_SPOTDIRECTION], 1, &spotDirection_cameraspace.x);
	}


	RenderSkybox();
	modelStack.PushMatrix();
	modelStack.Scale(100, 100, 100);
	modelStack.Translate(4, 0, 0);
	modelStack.PopMatrix();

	//Ground
	modelStack.PushMatrix();
	modelStack.Translate(0, -20, 0);
	modelStack.Scale(5, 5, 5);
	RenderMesh(meshList[GEO_MODEL1], false);
	modelStack.PopMatrix();

	//Sloped ground
	modelStack.PushMatrix();
	modelStack.Translate(-40, -21, 0);
	modelStack.Scale(5, 5, 9);
	modelStack.Rotate(90, 0, 1, 0);
	RenderMesh(meshList[GEO_MODEL2], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(0, -21, 40);
	modelStack.Scale(9, 5, 5);
	modelStack.Rotate(180, 0, 1, 0);
	RenderMesh(meshList[GEO_MODEL2], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(-40, -21, 40);
	modelStack.Scale(1, 5, 1);
	modelStack.Rotate(45, 0, -1, 0);
	RenderMesh(meshList[GEO_MODEL2], false);
	modelStack.PopMatrix();

	frame++;
	finaltime = time(NULL);
	if (finaltime - initialtime > 0)
	{
		fps = frame / (finaltime - initialtime);
		frame = 0;
		initialtime = finaltime;
	}

	modelStack.PushMatrix();
	std::stringstream ss2;
	ss2 << fps;
	std::string str2 = ss2.str();
	RenderTextOnScreen(meshList[GEO_FPS], "FPS: " + str2, Color(0, 1, 0), 3, 1, 2);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	std::stringstream ss3;
	for (int i = 0; i < camera.player_Stamina; i += 10)
	{
		ss3 << "|";
	}
	std::string str3 = ss3.str();
	RenderTextOnScreen(meshList[GEO_FPS], "Stamina: " + str3, Color(0, 1, 0), 3, 1, 5);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	RenderTextOnScreen(meshList[GEO_TEXT], "Press 'E' to enter the vehicle", Color(0, 1, 0), 2, activetext.x, activetext.y);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	RenderTextOnScreen(meshList[GEO_TEXT], "Press 'T' to leave the vehicle", Color(0, 1, 0), 2, activetext1.x, activetext1.y);
	modelStack.PopMatrix();

	

}

void SceneMain::RenderMesh(Mesh *mesh, bool enableLight)
{
	Mtx44 MVP, modelView, modelView_inverse_transpose;

	MVP = projectionStack.Top() * viewStack.Top() * modelStack.Top();
	glUniformMatrix4fv(m_parameters[U_MVP], 1, GL_FALSE, &MVP.a[0]);
	modelView = viewStack.Top() * modelStack.Top();
	glUniformMatrix4fv(m_parameters[U_MODELVIEW], 1, GL_FALSE, &modelView.a[0]);
	if (enableLight)
	{
		glUniform1i(m_parameters[U_LIGHTENABLED], 1);
		modelView_inverse_transpose = modelView.GetInverse().GetTranspose();
		glUniformMatrix4fv(m_parameters[U_MODELVIEW_INVERSE_TRANSPOSE], 1, GL_FALSE, &modelView_inverse_transpose.a[0]);

		//load material
		glUniform3fv(m_parameters[U_MATERIAL_AMBIENT], 1, &mesh->material.kAmbient.r);
		glUniform3fv(m_parameters[U_MATERIAL_DIFFUSE], 1, &mesh->material.kDiffuse.r);
		glUniform3fv(m_parameters[U_MATERIAL_SPECULAR], 1, &mesh->material.kSpecular.r);
		glUniform1f(m_parameters[U_MATERIAL_SHININESS], mesh->material.kShininess);
	}
	else
	{
		glUniform1i(m_parameters[U_LIGHTENABLED], 0);
	}

	if (mesh->textureID > 0)
	{
		glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED], 1);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, mesh->textureID);
		glUniform1i(m_parameters[U_COLOR_TEXTURE], 0);
	}
	else
	{
		glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED], 0);
	}
	mesh->Render(); //this line should only be called once 
	if (mesh->textureID > 0)
	{
		glBindTexture(GL_TEXTURE_2D, 0);
	}



}

void SceneMain::RenderSkybox()
{

	modelStack.PushMatrix();
	modelStack.Scale(300, 300, 300);
	modelStack.Translate(-0.495, 0, 0);
	modelStack.Rotate(90, 0, 1, 0);
	RenderMesh(meshList[GEO_LEFT], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Scale(300, 300, 300);
	modelStack.Translate(0.495, 0, 0);
	modelStack.Rotate(-90, 0, 1, 0);
	RenderMesh(meshList[GEO_RIGHT], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Scale(300, 300, 300);
	modelStack.Translate(0, 0.495, 0);
	modelStack.Rotate(-180, 0, 1, 0);
	modelStack.Rotate(90, 1, 0, 0);
	modelStack.Rotate(90, 0, 0, 1);
	RenderMesh(meshList[GEO_TOP], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Scale(300, 300, 300);
	modelStack.Translate(0, -0.495, 0);
	modelStack.Rotate(-90, 1, 0, 0);
	modelStack.Rotate(90, 0, 0, 1);
	RenderMesh(meshList[GEO_BOTTOM], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Scale(300, 300, 300);
	modelStack.Translate(0, 0, -0.495);
	RenderMesh(meshList[GEO_FRONT], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Scale(300, 300, 300);
	modelStack.Translate(0, 0, 0.495);
	modelStack.Rotate(180, 0, 1, 0);
	RenderMesh(meshList[GEO_BACK], false);
	modelStack.PopMatrix();

}

void SceneMain::RenderText(Mesh* mesh, std::string text, Color color)
{
	if (!mesh || mesh->textureID <= 0) //Proper error check
		return;

	glDisable(GL_DEPTH_TEST);
	glUniform1i(m_parameters[U_TEXT_ENABLED], 1);
	glUniform3fv(m_parameters[U_TEXT_COLOR], 1, &color.r);
	glUniform1i(m_parameters[U_LIGHTENABLED], 0);
	glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED], 1);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, mesh->textureID);
	glUniform1i(m_parameters[U_COLOR_TEXTURE], 0);
	for (unsigned i = 0; i < text.length(); ++i)
	{
		Mtx44 characterSpacing;
		characterSpacing.SetToTranslation(i * 1.0f, 0, 0); //1.0f is the spacing of each character, you may change this value
		Mtx44 MVP = projectionStack.Top() * viewStack.Top() * modelStack.Top() * characterSpacing;
		glUniformMatrix4fv(m_parameters[U_MVP], 1, GL_FALSE, &MVP.a[0]);

		mesh->Render((unsigned)text[i] * 6, 6);
	}
	glBindTexture(GL_TEXTURE_2D, 0);
	glUniform1i(m_parameters[U_TEXT_ENABLED], 0);
	glEnable(GL_DEPTH_TEST);
}

void SceneMain::RenderTextOnScreen(Mesh* mesh, std::string text, Color color, float size, float x, float y)
{
	if (!mesh || mesh->textureID <= 0) //Proper error check
		return;

	glDisable(GL_DEPTH_TEST);
	Mtx44 ortho;
	ortho.SetToOrtho(0, 80, 0, 60, -10, 10); //size of screen UI
	projectionStack.PushMatrix();
	projectionStack.LoadMatrix(ortho);
	viewStack.PushMatrix();
	viewStack.LoadIdentity(); //No need camera for ortho mode
	modelStack.PushMatrix();
	modelStack.LoadIdentity(); //Reset modelStack
	modelStack.Scale(size, size, size);
	modelStack.Translate(x, y, 0);
	glUniform1i(m_parameters[U_TEXT_ENABLED], 1);
	glUniform3fv(m_parameters[U_TEXT_COLOR], 1, &color.r);
	glUniform1i(m_parameters[U_LIGHTENABLED], 0);
	glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED], 1);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, mesh->textureID);
	glUniform1i(m_parameters[U_COLOR_TEXTURE], 0);
	for (unsigned i = 0; i < text.length(); ++i)
	{
		Mtx44 characterSpacing;
		characterSpacing.SetToTranslation(i * 1.0f, 0, 0); //1.0f is the spacing of each character, you may change this value
		Mtx44 MVP = projectionStack.Top() * viewStack.Top() * modelStack.Top() * characterSpacing;
		glUniformMatrix4fv(m_parameters[U_MVP], 1, GL_FALSE, &MVP.a[0]);

		mesh->Render((unsigned)text[i] * 6, 6);
	}
	glBindTexture(GL_TEXTURE_2D, 0);
	glUniform1i(m_parameters[U_TEXT_ENABLED], 0);
	projectionStack.PopMatrix();
	viewStack.PopMatrix();
	modelStack.PopMatrix();

	glEnable(GL_DEPTH_TEST);

}


void SceneMain::Exit()
{

	for (int i = 0; i < NUM_GEOMETRY; ++i)
	{
		if (meshList[i] != NULL)
		{
			delete meshList[i];
		}
		meshList[i] = NULL;
	}
	// Cleanup VBO here

	glDeleteProgram(m_programID);

}

