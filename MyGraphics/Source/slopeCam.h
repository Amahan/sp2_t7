#ifndef SLOPE_CAM_H
#define SLOPE_CAM_H

#include "CollisionChecker.h"
#include "Camera.h"
#include "Application.h"
class SlopeCam : public Camera
{
public:
	Vector3 view;
	Vector3 up;
	Vector3 right;

	Vector3 defaultPosition;
	Vector3 defaultTarget;
	Vector3 defaultUp;

	float Xyaw;
	float Ypitch;
	bool firstrun;
	double xmousepos;
	double ymousepos;
	double oldmousepos;
	float yaw;
	float pitch;
	double player_Stamina;

	SlopeCam();
	~SlopeCam();
	virtual void Init(const Vector3& pos, const Vector3& target, const Vector3& up);
	virtual void Update(double dt);
	virtual void Reset();
	void MouseMovement(double dt);
	float totalYaw;
	double yMouse, xMouse;
	float CameraYaw;
};

#endif