#ifndef CAMERA_3_H
#define CAMERA_3_H

#include "Camera.h"

class CameraMain : public Camera
{
public:
	//Vector3 position;
	//Vector3 target;
	//Vector3 up;

	Vector3 defaultPosition;
	Vector3 defaultTarget;
	Vector3 defaultUp;
	Vector3 view;
	Vector3 right;
	float Xyaw;
	float Ypitch;
	bool firstrun;
	double xmousepos;
	double ymousepos;
	double oldmousepos;
	float yaw;
	float pitch;
	double player_Stamina;

	CameraMain();
	~CameraMain();
	virtual void Init(const Vector3& pos, const Vector3& target, const Vector3& up);
	virtual void Update(double dt);
	virtual void Reset();
	
};

#endif