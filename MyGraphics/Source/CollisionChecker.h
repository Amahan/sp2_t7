#pragma once
#include "Camera.h"
#include "Application.h"

class CollisionChecker
{
private:

public:
	CollisionChecker();
	~CollisionChecker();
	//TODO: Return a valid position for objects to move
	void addCollisionCube(Vector3 position, Vector3 direction);
	Vector3 checkCollision(Vector3, Vector3);
	static bool checkGravity(Vector3&, float);
};

