#ifndef BOX_H
#define BOX_H

#include "Vector3.h"

struct Box
{
	Vector3 position;

	float minX;
	float minY;
	float minZ;
	float maxX;
	float maxY;
	float maxZ;

	Box(Vector3 _position, float _size)
	{
		position = _position;
		minX = position.x - _size;
		maxX = position.x + _size;
		minY = position.y - _size;
		maxY = position.y + _size;
		minZ = position.z - _size;
		maxZ = position.z + _size;
	}

	Box(Vector3 _position, float _length, float _height, float _width)
	{
		position = _position;
		minX = position.x - _length;
		maxX = position.x + _length;
		minY = position.y - _height;
		maxY = position.y + _height;
		minZ = position.z - _width;
		maxZ = position.z + _width;
	}
};



#endif